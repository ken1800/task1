const arr = [1, 2, 3, 4, 5];
const targ = 9;

const arrFunc = (array, targetSum) => {
  let empt = {};
  for (let i = 0; i < array.length; i++) {
    const currentElement = array[i];
    const difference = targetSum - currentElement;

    if (empt[difference]) {
      return [difference, currentElement];
    } else {
      empt[currentElement] = true;
    }
  }
  return [];
};

console.log(arrFunc(arr, targ));
